import numpy as np
from keras.datasets import mnist


class DataLoader:

    @staticmethod
    def load_mnist():
        (train_inputs, train_outputs), (test_inputs, test_outputs) = mnist.load_data()
        # Масштабируем данные от 0.01 до 0.99
        train_inputs = train_inputs / 255.0 * 0.99 + 0.01
        test_inputs = test_inputs / 255.0 * 0.99 + 0.01
        outputs_nodes = len(set(train_outputs))
        outputs_matrix = np.zeros(outputs_nodes) + 0.01

        train_outputs_list = []
        for output_label in train_outputs.copy():
            output = outputs_matrix.copy()
            output[output_label] = 0.99
            train_outputs_list.append(output)
        train_outputs = np.array(train_outputs_list)

        test_outputs_list = []
        for output_label in test_outputs.copy():
            output = outputs_matrix.copy()
            output[output_label] = 0.99
            test_outputs_list.append(output)
        test_outputs = np.array(test_outputs_list)

        input_nodes = train_inputs[0].shape[0] * train_inputs[0].shape[1]
        return train_inputs, train_outputs, test_inputs, test_outputs, input_nodes, outputs_nodes
