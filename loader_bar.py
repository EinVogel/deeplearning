import time
from progress.bar import IncrementalBar


class LoadIndicator:

    def __init__(self, items, work_name):
        self.iterator = IncrementalBar(work_name, max=len(items))
        self.time_start = time.time()
        self.time_stop = None

    def next(self):
        self.iterator.next()

    def finish(self):
        self.iterator.finish()
        self.time_stop = time.time()
        print('Время выполнения', round(self.time_stop - self.time_start, 1), 'сек')