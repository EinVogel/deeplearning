import numpy as np
from typing import Optional


def gradient_boosting(
        input_matrix: np.array,
        weight_matrix: np.array,
        goal_output: int,
        alpha: float,
        iterates: int
) -> Optional[np.array]:
    """Обучение вечов с помощью метода градиентного спуска"""
    for iteration in range(iterates):
        assert input_matrix.shape[0] == weight_matrix.T.shape[0], 'Приведите матрицы к правильной форме'
        prediction_output = input_matrix.dot(weight_matrix.T)
        error = (prediction_output - goal_output) ** 2
        derivative = input_matrix * (prediction_output - goal_output)
        weight_matrix = weight_matrix - (alpha * derivative)
        print("loss:" + str(error) + " Prediction:" + str(prediction_output))
    return weight_matrix


