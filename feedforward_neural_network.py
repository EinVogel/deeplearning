import numpy as np
from scipy import special


class FeedForwardNeuralNetwork:
    """Полносвязанная нейронная сеть"""

    #  инициализировать нейронную сеть
    def __init__(self, input_nodes, hidden_nodes, output_nodes, learning_rate):
        # задать количество узлов во входном, скрытом и выходном слое
        self.input_nodes = input_nodes
        self.hidden_nodes = hidden_nodes
        self.output_nodes = output_nodes
        # коэффициент обучения
        self.learning_rate = learning_rate
        # Матрицы весовых коэффициентов связей weights_input_hidden (между входным и скрытым
        #  слоями) и weights_hidden_output (между скрытым и выходным слоями).
        self.weights_input_hidden = (np.random.rand(self.hidden_nodes, self.input_nodes) - 0.5)
        self.weights_hidden_output = (np.random.rand(self.output_nodes, self.hidden_nodes) - 0.5)
        # использование сигмоиды в качестве функции активации
        self.activation_function = lambda x: special.expit(x)

    def training_iteration(self,
                           inputs_list: list,
                           targets_list: list):
        """Прохождение одной тренировочной итерации"""
        # преобразовать списоки входных значений и ответы тренировочных примеров в матрицы
        inputs = np.array(inputs_list, ndmin=2).T
        targets = np.array(targets_list, ndmin=2).T

        # рассчитать входящие сигналы для скрытого слоя
        hidden_inputs = np.dot(self.weights_input_hidden, inputs)
        # рассчитать исходящие сигналы для скрытого слоя
        hidden_outputs = self.activation_function(hidden_inputs)
        # breakpoint()

        # рассчитать входящие сигналы для выходного слоя
        final_inputs = np.dot(self.weights_hidden_output, hidden_outputs)
        # рассчитать исходящие сигналы для выходного слоя
        final_outputs = self.activation_function(final_inputs)

        # ошибки выходного слоя = (целевое значение - фактическое значение)
        output_errors = targets - final_outputs
        # ошибки скрытого слоя - это ошибки output_errors,
        # распределенные пропорционально весовым коэффициентам связей
        # и рекомбинированные на скрытых узлах
        hidden_errors = np.dot(self.weights_hidden_output.T, output_errors)
        # обновить весовые коэффициенты для связей между
        #  скрытым и выходным слоями
        self.weights_hidden_output += self.learning_rate * np.dot(
            (output_errors * final_outputs * (1.0 - final_outputs)),
            np.transpose(hidden_outputs))
        # обновить весовые коэффициенты для связей между
        #  входным и скрытым слоями
        self.weights_input_hidden += self.learning_rate * np.dot(
            (hidden_errors * hidden_outputs * (1.0 - hidden_outputs)),
            np.transpose(inputs))

    def train_model(self,
                    train_inputs: np.array,
                    train_outputs: np.array,
                    iterates: int):
        for iteration in range(iterates):
            print('iteration')
            for input_matrix, output_matrix in zip(train_inputs, train_outputs):
                input_matrix = input_matrix.reshape(self.input_nodes)
                self.training_iteration(input_matrix, output_matrix)

    def query(self, input_matrix):
        """Опрос нейронной сети"""
        # преобразовать список входных значений в двухмерный массив
        inputs = input_matrix.reshape(self.input_nodes)
        # рассчитать входящие сигналы для скрытого слоя
        hidden_inputs = np.dot(self.weights_input_hidden, inputs)
        # рассчитать исходящие сигналы для скрытого слоя
        hidden_outputs = self.activation_function(hidden_inputs)
        # рассчитать входящие сигналы для выходного слоя
        final_inputs = np.dot(self.weights_hidden_output, hidden_outputs)
        # рассчитать исходящие сигналы для выходного слоя
        final_outputs = self.activation_function(final_inputs)
        print(np.argmax(final_outputs))

        return final_outputs

